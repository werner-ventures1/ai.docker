FROM gcr.io/deeplearning-platform-release/base-cu101
RUN wget https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb && \
    dpkg --purge packages-microsoft-prod && \
    dpkg -i packages-microsoft-prod.deb && \
    apt update -y && \
    apt install -y openmpi-bin libopenmpi-dev libhdf5-openmpi-dev apt-transport-https && \
    apt-get install -y --no-install-recommends build-essential dotnet-sdk-3.1 powershell && \
    rm -r /tmp/*
RUN apt-get install -y --no-install-recommends clang mono-complete ca-certificates curl file g++ git locales make uuid-runtime && \
    conda update --all && \
    rm -rf /var/lib/apt/lists/*
RUN conda install libgcc setuptools cmake jupyterlab jupyterlab-sos libsndfile mkl zeromq pip && \
    export SETUPTOOLS_USE_DISTUTILS=stdlib && \
    pip install --no-cache-dir --platform modin[all] wheel onnxruntime-gpu pycparser nimbusml pycuda horovod nvidia-pyindex && \
    conda clean -a && \
    rm -r /tmp/*
RUN pip install --no-cache-dir --platform jupyter_c_kernel hummingbird-ml && \
    rm -r /tmp/*
RUN pip install --no-cache-dir --platform fastai h2o ffmpeg chaostoolkit chaostoolkit-google-cloud-platform && \
    install_c_kernel && \
    rm -r /tmp/*
RUN wget https://dist.nuget.org/win-x86-commandline/latest/nuget.exe && \
    dotnet new --install Boxed.Templates && \
    dotnet new nuget --name "N30"
WORKDIR "/N30/Source/N30"
RUN dotnet add package FSharp.Core && \
    dotnet add package Xamarin.Essentials && \
    dotnet add package Microsoft.ML && \
    dotnet add package Microsoft.ML.OnnxRuntime.Gpu && \
    dotnet add package Microsoft.ML.TensorFlow && \
    dotnet add package Google.Cloud.Firestore && \
    dotnet tool install -g --add-source "https://dotnet.myget.org/F/dotnet-try/api/v3/index.json" Microsoft.dotnet-interactive && \
    rm -r /tmp/* && \
    curl https://sh.rustup.rs -sSf | bash -s -- -y 
ENV PATH="$PATH:/root/.dotnet/tools:/root/.cargo/bin"
RUN dotnet interactive jupyter install && \
    dotnet nuget locals all -c &&  \
    cargo install sccache && \
    cargo install evcxr_jupyter && \
    cargo install cargo-script && \
    pip install --platform rust-magic && \
    evcxr_jupyter --install